# Angular Workspace

The code repository for the Angular Workspace course.

## Workspace Set Up

```ts
npx create-nx-workspace workspace --npm-scope=buildmotion
```

It will use the *interactive* option to provide you with some options to select.

* empty: create an *empty* workspace
* CLI: select the *Angular CLI* option
* Nx distributed caching: *No*

```ts
CREATE workspace/nx.json (513 bytes)
CREATE workspace/tsconfig.json (509 bytes)
CREATE workspace/package.json (1207 bytes)
CREATE workspace/README.md (3404 bytes)
CREATE workspace/.editorconfig (245 bytes)
CREATE workspace/.gitignore (503 bytes)
CREATE workspace/.prettierignore (74 bytes)
CREATE workspace/.prettierrc (26 bytes)
CREATE workspace/angular.json (96 bytes)
CREATE workspace/decorate-angular-cli.js (3093 bytes)
CREATE workspace/tools/tsconfig.tools.json (218 bytes)
CREATE workspace/tools/schematics/.gitkeep (0 bytes)
CREATE workspace/apps/.gitkeep (0 bytes)
CREATE workspace/libs/.gitkeep (0 bytes)
√ Packages installed successfully.