import { TestBed } from '@angular/core/testing';

import { CreateAccountStateMachineService } from './create-account-state-machine.service';

describe('CreateAccountStateMachineService', () => {
  let service: CreateAccountStateMachineService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CreateAccountStateMachineService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
