import { LoggingService, Severity } from '@valencia/logging';
import { State, StateService } from '@valencia/state-machine';

import { CreateAccountMachineConfigurationContext } from './create-account-machine-configuration-context';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { ServiceBase } from '@valencia/foundation';

@Injectable({
  providedIn: 'root',
})
export class CreateAccountStateMachineService extends ServiceBase {
  public readonly currentState$ = this.stateService.currentState$;
  public readonly transition$ = this.stateService.transition$;

  private routePrefix: string;

  constructor(
    private router: Router,
    private stateService: StateService,
    private machineConfig: CreateAccountMachineConfigurationContext,
    loggingService: LoggingService
  ) {
    super('CreateAccountStateMachineService', loggingService);
    this.init();
  }

  initialize(routePrefix?: string) {
    this.routePrefix = routePrefix;
    if (this.machineConfig && this.machineConfig.config) {
      this.loggingService.log(this.serviceName, Severity.Information, `Initializing q: ${this.machineConfig.config.name}/${this.machineConfig.config.id}`);

      // init and load initial state;
      this.stateService.start(this.machineConfig.config);
    } else {
      this.loggingService.log(this.serviceName, Severity.Error, `The [state machine service] does not contain a valid configuration.`);
    }
  }

  transition($event: any) {
    if ($event) {
      this.stateService.send($event);
    }
  }

  private init() {
    this.currentState$.subscribe((response) => {
      if (response && response.context) {
        this.loadStateComponent(response);
      }
    });
  }

  private loadStateComponent(currentState: State<any>) {
    try {
      this.router.navigateByUrl(`${this.routePrefix}/${currentState.context}`);
    } catch (error) {
      this.handleError(error);
    }
  }
}
