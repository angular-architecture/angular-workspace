import { Guid } from 'guid-typescript';
import { IStateMachineConfig } from '@valencia/state-machine';
import { StateType } from 'libs/state-machine/src/lib/state-type.enum';

export const stateMachineConfig: IStateMachineConfig<string> = {
  actions: [],
  context: 'create-account-flow',
  id: Guid.create().toString(),
  initialState: 'start',
  name: 'new-account',
  states: [
    {
      context: 'create-account',
      entry: [],
      events: [
        {
          name: 'next',
          target: 'verify-account',
        },
      ],
      exit: [],
      id: Guid.create().toString(),
      name: 'start',
      stateType: StateType.start,
      transitions: [],
    },
    {
      context: 'verify-account',
      entry: [],
      events: [],
      exit: [],
      id: Guid.create().toString(),
      name: 'verify-account',
      stateType: StateType.Movement,
      transitions: [],
    },
    {
      context: 'login',
      entry: [],
      events: [],
      exit: [],
      id: Guid.create().toString(),
      name: 'login',
      stateType: StateType.finale,
      transitions: [],
    },
  ],
};
