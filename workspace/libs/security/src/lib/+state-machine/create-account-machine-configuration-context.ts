import { IStateMachineConfig } from '@valencia/state-machine';

export class CreateAccountMachineConfigurationContext {
  config: IStateMachineConfig<string>;
}
