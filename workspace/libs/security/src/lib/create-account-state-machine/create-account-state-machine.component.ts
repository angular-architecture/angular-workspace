import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { LoggingService, Severity } from '@valencia/logging';

import { ComponentBase } from '@valencia/foundation';
import { CreateAccountStateMachineService } from '../+state-machine/create-account-state-machine.service';

@Component({
  selector: 'buildmotion-create-account-state-machine',
  templateUrl: './create-account-state-machine.component.html',
  styleUrls: ['./create-account-state-machine.component.scss'],
  providers: [CreateAccountStateMachineService],
})
export class CreateAccountStateMachineComponent extends ComponentBase implements OnInit {
  stateMachineName: any;
  constructor(private machineService: CreateAccountStateMachineService, private route: ActivatedRoute, loggingService: LoggingService, router: Router) {
    super('CreateAccountStateMachineComponent', loggingService, router);
  }

  ngOnInit(): void {
    this.retrieveStateMachineName();
    this.machineService.initialize(`security/${this.stateMachineName}`);
  }

  private retrieveStateMachineName() {
    try {
      this.loggingService.log(this.componentName, Severity.Information, `Preparing to retrieve [name] for State Machine.`);
      this.stateMachineName = this.route.snapshot.paramMap.get('name');
      this.loggingService.log(this.componentName, Severity.Information, `State Machine name is [${this.stateMachineName}].`);
    } catch (error) {
      this.logError(error, `Error while attempting to retrieve [state machine name] from route.`);
    }
  }
}
