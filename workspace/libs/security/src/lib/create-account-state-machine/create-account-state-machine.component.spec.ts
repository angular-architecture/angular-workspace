import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateAccountStateMachineComponent } from './create-account-state-machine.component';

describe('CreateAccountStateMachineComponent', () => {
  let component: CreateAccountStateMachineComponent;
  let fixture: ComponentFixture<CreateAccountStateMachineComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateAccountStateMachineComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateAccountStateMachineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
