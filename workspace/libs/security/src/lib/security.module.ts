import { CommonModule } from '@angular/common';
import { CreateAccountMachineConfigurationContext } from './+state-machine/create-account-machine-configuration-context';
import { CreateAccountStateMachineComponent } from './create-account-state-machine/create-account-state-machine.component';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { stateMachineConfig } from './+state-machine/create-account.config';

@NgModule({
  imports: [
    CommonModule,

    RouterModule.forChild([
      {
        path: ':name',
        component: CreateAccountStateMachineComponent,
        children: [
          {
            path: 'create-account',
            loadChildren: () => import('./create-account/create-account.module').then((m) => m.CreateAccountModule),
          },
          { path: 'verify-account', loadChildren: () => import('./verify-account/verify-account.module').then((m) => m.VerifyAccountModule) },
          { path: 'login', loadChildren: () => import('./login/login.module').then((m) => m.LoginModule) },
        ],
      },
      { path: 'change-password', loadChildren: () => import('./change-password/change-password.module').then((m) => m.ChangePasswordModule) },
      { path: 'create-new-account', loadChildren: () => import('./create-account/create-account.module').then((m) => m.CreateAccountModule) },
      { path: 'forgot-password', loadChildren: () => import('./forgot-password/forgot-password.module').then((m) => m.ForgotPasswordModule) },
      { path: 'logout', loadChildren: () => import('./logout/logout.module').then((m) => m.LogoutModule) },
      { path: 'login', loadChildren: () => import('./login/login.module').then((m) => m.LoginModule) },
      { path: 'verify-account', loadChildren: () => import('./verify-account/verify-account.module').then((m) => m.VerifyAccountModule) },
    ]),
  ],
  declarations: [CreateAccountStateMachineComponent],
  exports: [],
  providers: [
    {
      provide: CreateAccountMachineConfigurationContext,
      useValue: { config: stateMachineConfig },
    },
  ],
})
export class SecurityModule {}
