import { IStateMachineConfig, MachineAction, State } from '@valencia/state-machine';

import { Guid } from 'guid-typescript';
import { StateType } from 'libs/state-machine/src/lib/state-type.enum';

export const qConfig: IStateMachineConfig<string> = {
  actions: [],
  context: 'abc-123',
  id: Guid.create().toString(),
  initialState: 'start',
  name: 'abc-123',
  states: [
    {
      context: 'email-address',
      entry: [
        // actions... --> execute by 'getCreditRating'
      ],
      events: [
        {
          name: 'next',
          target: 'password',
          // condition: {//code/data --> isHomeOwner=true --> loanType}
        },
      ],
      exit: [],
      id: Guid.create().toString(),
      name: 'start',
      stateType: StateType.start,
      transitions: [],
    },
    {
      context: 'password',
      entry: [],
      events: [
        {
          name: 'previous',
          target: 'email-address',
        },
        {
          name: 'next',
          target: 'verify-account',
        },
      ],
      exit: [],
      id: Guid.create().toString(),
      name: 'password',
      stateType: StateType.Movement,
      transitions: [],
    },
    {
      context: 'verify-account',
      entry: [],
      events: [
        {
          name: 'previous',
          target: 'password',
        },
        {
          name: 'next',
          target: 'contact-info',
        },
      ],
      exit: [],
      id: Guid.create().toString(),
      name: 'verify-account',
      stateType: StateType.Movement,
      transitions: [],
    },
    {
      context: 'contact-info',
      entry: [],
      events: [
        {
          name: 'next',
          target: 'company-info',
        },
      ],
      exit: [],
      id: Guid.create().toString(),
      name: 'contact-info',
      stateType: StateType.Movement,
      transitions: [],
    },
    {
      context: 'company-info',
      entry: [],
      events: [
        {
          name: 'previous',
          target: 'contact-info',
        },
      ],
      exit: [],
      id: Guid.create().toString(),
      name: 'contact-info',
      transitions: [],
      stateType: StateType.finale,
    },
  ],
};
