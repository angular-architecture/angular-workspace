export enum StateType {
  /**
   * Use to indicate that it is the start or first member of the
   * state. It is typically the initial state item of the
   * machine.
   */
  start = 'start',
  /**
   * Use to indicate the state item is the last or final state
   * of the state machine. There are no other state to transition to.
   */
  finale = 'finale',
  /**
   * The default type for a state item.
   */
  Movement = 'Movement',
}
