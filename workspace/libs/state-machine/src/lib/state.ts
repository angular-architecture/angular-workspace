import { Guid } from 'guid-typescript';
import { MachineAction } from './machine-action';
import { MachineEvent } from './machine-event';
import { StateType } from './state-type.enum';
import { Transition } from './transition';

export class State<T> {
  // actions:
  context: T;
  entry: Array<MachineAction> = [];
  events: MachineEvent[] = [];
  exit: Array<MachineAction> = [];
  id: string;
  name: string;
  stateType: StateType = StateType.Movement;
  transitions: Transition[] = [];

  constructor(name: string) {
    this.name = name;
    this.id = Guid.create().toString();
  }
}
