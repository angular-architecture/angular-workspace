import { HttpClientModule } from '@angular/common/http';
import { LoggingService } from '@valencia/logging';
import { Machine } from './machine';
import { State } from './state';
import { StateService } from './state.service';
import { TestBed } from '@angular/core/testing';

let context: any;
describe('StateService with Light Machine', () => {
  beforeAll(() => {
    context = new Machine('Light');
    const state = new State('green');
    context.states.push(state);
    context.initialState = state;
  });

  afterAll(() => {
    console.log(JSON.stringify(context));
  });

  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [
        {
          provide: LoggingService,
          useClass: LoggingService,
        },
      ],
    })
  );

  it('should not create machine with [invalid] machine state name', () => {
    const context = new Machine(undefined);
    const service: StateService = TestBed.inject(StateService);
    expect(service).toBeTruthy();

    service.start(context);

    expect(service.machine.name).toBe(undefined);
  });

  it('should create machine with [valid] initial state', () => {
    const service: StateService = TestBed.inject(StateService);

    service.start(context);

    expect(service).toBeTruthy();
    expect(service.machine).not.toBe(null);
    expect(service.machine.initialState).not.toBeNull();
  });

  it('should create machine with name', () => {
    const service: StateService = TestBed.inject(StateService);
    expect(service).toBeTruthy();

    service.start(context);

    expect(service.machine).not.toBe(null);
    expect(service.machine.name).not.toBeUndefined();
  });

  it('should create machine with valid id', () => {
    const service: StateService = TestBed.inject(StateService);
    expect(service).toBeTruthy();

    service.start(context);

    expect(service.machine).not.toBe(null);
    expect(service.machine.id).not.toBeUndefined();
  });

  it('should create machine with valid id', () => {
    const service: StateService = TestBed.inject(StateService);
    expect(service).toBeTruthy();

    service.start(context);

    expect(service.machine).not.toBe(null);
    expect(service.machine.id).not.toBeUndefined();
  });

  it('should add new state to context', () => {
    const service: StateService = TestBed.inject(StateService);
    expect(service).toBeTruthy();

    service.start(context);
    const yellowState = new State('yellow');
    service.addState(yellowState);

    expect(service.machine).not.toBe(null);
    expect(service.machine.id).not.toBeUndefined();
  });
});
