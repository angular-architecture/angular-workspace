import { Injectable, Inject } from '@angular/core';
import { ApiResponse } from '@valencia/common';

import { ServiceBase } from '@valencia/foundation';
import { LoggingService, Severity } from '@valencia/logging';
import { BehaviorSubject, Observable } from 'rxjs';
import { BusinessProviderService } from './business/business-provider.service';
import { Machine } from './machine';
import { State } from './state';

@Injectable({
  providedIn: 'root',
})
export class StateService extends ServiceBase {
  private currentState: State<any>;

  private currentStateSubject: BehaviorSubject<State<any>> = new BehaviorSubject<State<any>>(null);
  private transitionSubject: BehaviorSubject<State<any>> = new BehaviorSubject<State<any>>(null);

  public readonly currentState$: Observable<State<any>> = this.currentStateSubject.asObservable();
  public readonly transition$: Observable<State<any>> = this.transitionSubject.asObservable();
  machine: Machine<any> = undefined;

  constructor(@Inject(BusinessProviderService) private businessProvider: BusinessProviderService, loggingService: LoggingService) {
    super('StateService', loggingService);
    this.businessProvider.serviceContext = this.serviceContext;
  }

  start<TContext>(machine: Machine<TContext>) {
    this.loggingService.log(this.serviceName, Severity.Information, `Preparing to create state-machine context.`);

    this.businessProvider.validateMachine<boolean, TContext>(machine).subscribe(
      (response) => this.handleValidateMachineResponse<TContext>(response, machine),
      (error) => this.handleValidateMachineError(error),
      () => this.finishValidateMachine()
    );
  }

  /**
   * Use to initiate a transition sequence.
   *
   * @param eventName The name of the target event for the current state.
   */
  send(eventName: string) {
    this.exitCurrentState();
    this.retrieveTransitionTargetState(eventName);
  }

  private exitCurrentState() {
    // this.currentState.activities;
  }

  /**
   * Use to retrieve the initial state from the state configuration.
   */
  private retrieveInitialState(): void {
    this.businessProvider.retrieveInitialState<State<any>>(this.machine).subscribe(
      (response) => this.handleRetrieveInitialState(response),
      (error) => this.handleRetrieveInitialStateError(error),
      () => this.finishRetrieveInitialState()
    );
  }

  private retrieveTransitionTargetState(eventName: string) {
    this.businessProvider.retrieveTransitionTargetState<State<any>>(eventName, this.machine, this.currentState).subscribe(
      (response) => this.handleRetrieveTransitionState(response),
      (error) => this.handleRetrieveTransitionStateError(error),
      () => this.finishRetrieveTransitionState()
    );
  }

  private finishValidateMachine(): void {
    this.loggingService.log(this.serviceName, Severity.Information, `Finished validating machine configuration.`);
  }

  private handleValidateMachineError(error: any): void {
    this.handleError(error);
    throw new Error('The machine configuration is not valid.');
  }

  private handleValidateMachineResponse<TContext>(response: ApiResponse<boolean>, machine: Machine<TContext>): void {
    this.loggingService.log(this.serviceName, Severity.Information, `Preparing to process API response for [machine validation]`);
    if (response) {
      if (response.isSuccess) {
        this.loggingService.log(this.serviceName, Severity.Information, `Preparing to process [successful] API response`);
        this.machine = machine;
        this.retrieveInitialState();
      } else {
        this.loggingService.log(this.serviceName, Severity.Information, `Preparing to process [unsuccessful] API response`);
      }
    }
  }

  private handleRetrieveTransitionState(response: ApiResponse<State<any>>): void {
    this.loggingService.log(this.serviceName, Severity.Information, `Preparing to process API response for [retrieving transition state]`);
    if (response) {
      if (response.isSuccess && response.data) {
        this.loggingService.log(this.serviceName, Severity.Information, `Preparing to process [successful] API response`);
        this.transitionSubject.next(this.currentState);
        this.setCurrentState(response);
      } else {
        this.loggingService.log(this.serviceName, Severity.Information, `Preparing to process [unsuccessful] API response`);
      }
    }
  }

  private handleRetrieveTransitionStateError(error: any): void {
    this.handleError(error);
  }

  private finishRetrieveTransitionState(): void {
    this.loggingService.log(
      this.serviceName,
      Severity.Information,
      `Finished processing request to retrieve transition state. Current state is: ${this.currentState.name}`
    );
  }

  private finishRetrieveInitialState(): void {
    this.loggingService.log(this.serviceName, Severity.Information, `Finished processing request for the initial state.`);
  }

  private handleRetrieveInitialStateError(error: any): void {
    this.logError(error, 'Error while attempting to retrieve initial state.');
  }

  private handleRetrieveInitialState(response: ApiResponse<State<any>>) {
    this.loggingService.log(this.serviceName, Severity.Information, `Preparing to process API response for [initial state]`);
    if (response) {
      if (response.isSuccess) {
        this.setCurrentState(response);
        this.loggingService.log(this.serviceName, Severity.Information, `Loading initial state: ${this.currentStateSubject.value.name}.`);
      } else {
        this.loggingService.log(this.serviceName, Severity.Information, `Preparing to process [unsuccessful] API response`);
      }
    }
  }

  private setCurrentState(response: ApiResponse<State<any>>) {
    this.currentState = response.data;
    this.currentStateSubject.next(this.currentState);
  }
}
