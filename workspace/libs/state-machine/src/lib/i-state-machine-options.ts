import { MachineAction } from './machine-action';

export interface IStateMachineOptions<TContext> {
  actions: MachineAction[];
  context: TContext;
}
