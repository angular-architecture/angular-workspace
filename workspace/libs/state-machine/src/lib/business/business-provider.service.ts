import { Injectable, Inject } from '@angular/core';
import { Observable, of } from 'rxjs';

import { ConfigurationService } from '@valencia/configuration';
import { LoggingService } from '@valencia/logging';
import { ServiceBase } from '@valencia/foundation';
import { ApiMessage, ApiResponse } from '@valencia/common';
import { HttpStateRepositoryService } from './http-state-repository.service';
import { IBusinessProviderService } from './i-business-provider.service';
import { Machine } from '../machine';
import { State } from '../state';
import { RetrieveInitialStateAction } from './actions/retrieve-initial-state.action';
import { RetrieveTransitionTargetStateAction } from './actions/retrieve-transition-target-state.action';
import { ValidateMachineAction } from './actions/validate-machine.action';

@Injectable({
  providedIn: 'root',
})
export class BusinessProviderService extends ServiceBase implements IBusinessProviderService {
  constructor(
    @Inject(HttpStateRepositoryService) public apiService: HttpStateRepositoryService,
    public configService: ConfigurationService,
    loggingService: LoggingService
  ) {
    super('BusinessProviderService', loggingService);
  }

  // someMethod<T>(someInput: string): Observable<ApiResponse<T>> {
  //   const action = new SomeAction<T>(someInput);
  //   action.Do(this);
  //   return action.response;
  // }

  createSuccessResponse<T>(data: any, message?: string): Observable<ApiResponse<T>> {
    const apiResponse = new ApiResponse<T>();
    apiResponse.isSuccess = true;
    apiResponse.data = data;
    apiResponse.message = message ? message : `Successfully processed request.`;

    return of(apiResponse);
  }

  // createFailResponse<T>(data: any, messages?: ApiMessage[], message?: string): Observable<ApiResponse<T>> {}

  retrieveInitialState<T>(machine: Machine<any>): Observable<ApiResponse<T>> {
    const action = new RetrieveInitialStateAction<T>(machine);
    action.Do(this);
    return action.response;
  }

  retrieveTransitionTargetState<T>($event: string, machine: Machine<any>, currentState: State<any>): Observable<ApiResponse<T>> {
    const action = new RetrieveTransitionTargetStateAction<T>($event, machine, currentState);
    action.Do(this);
    return action.response;
  }

  validateMachine<T, TContext>(machine: Machine<TContext>) {
    const action = new ValidateMachineAction<T>(machine);
    action.Do(this);
    return action.response;
  }
}
