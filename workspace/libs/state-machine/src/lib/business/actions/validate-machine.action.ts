import { BusinessActionBase } from './business-action-base';
import { MachineIsValidRule } from '../rules/machine-is-valid.rule';
import { Severity } from '@valencia/logging';

/**
 * Use this action to perform business logic with validation and business rules.
 */
export class ValidateMachineAction<T> extends BusinessActionBase<T> {
  constructor(private machine) {
    super('ValidateMachineAction');
  }

  /**
   * Use the [preValidateAction] to add any business or validation rules that
   * are required to pass in order to perform the action.
   *
   * Use the [ValidationContext] item of the action to add rules. The ValidationContext
   * uses a Fluent API to allow for chained rules to be configured.
   */
  preValidateAction() {
    this.validationContext.addRule(new MachineIsValidRule('MachineIsValid', 'The machine is not valid.', this.machine, this.showRuleMessages));
  }

  /**
   * Use the [performAction] operation to execute the target of the action's business logic. This
   * will only run if the rules and validations are successful.
   */
  performAction() {
    this.loggingService.log(this.actionName, Severity.Information, `Preparing to call API to complete action.`);
    this.response = this.businessProvider.createSuccessResponse(this.validationContext.isValid, 'The state machine configuration is valid.');
  }
}
