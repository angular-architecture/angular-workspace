import { IsNotNullOrUndefined, StringIsNotNullEmptyRange } from '@valencia/rules-engine';

import { BusinessActionBase } from './business-action-base';
import { Machine } from '../../machine';
import { MachineEvent } from '../../machine-event';
import { MachineIsValidRule } from '../rules/machine-is-valid.rule';
import { Severity } from '@valencia/logging';
import { State } from '../../state';
import { StateIsValidRule } from '../rules/state-is-valid.rule';

/**
 * Use this action to perform business logic with validation and business rules.
 */
export class RetrieveTransitionTargetStateAction<T> extends BusinessActionBase<T> {
  private currentEvent: MachineEvent;
  nextState: State<any>;

  constructor(private eventName: string, private machine: Machine<any>, private state: State<any>) {
    super('RetrieveTransitionTargetStateAction');
  }

  /**
   * Use the [preValidateAction] to add any business or validation rules that
   * are required to pass in order to perform the action.
   *
   * Use the [ValidationContext] item of the action to add rules. The ValidationContext
   * uses a Fluent API to allow for chained rules to be configured.
   */
  preValidateAction() {
    this.validationContext.addRule(
      new StringIsNotNullEmptyRange(
        'EventNameIsValid',
        'The event name is not valid. Unable to find event.',
        this.eventName,
        1,
        Number.MAX_SAFE_INTEGER,
        this.showRuleMessages
      )
    );

    this.validationContext.addRule(new MachineIsValidRule('MachineIsValid', 'The machine context is not valid.', this.machine, this.showRuleMessages));

    this.currentEvent = this.state.events.find((e) => e.name === this.eventName);

    this.validationContext.addRule(
      new IsNotNullOrUndefined('EventIsFound', 'Failed to find the event [] in the current state configuration.', this.currentEvent, this.showRuleMessages)
    );

    if (this.currentEvent) {
      this.nextState = this.machine.states.find((s) => s.context === this.currentEvent.target);
      this.validationContext.addRule(
        new StateIsValidRule('NextStateIsValid', `Failed to find the target state with [${this.currentEvent.target}].`, this.nextState, this.showRuleMessages)
      );
    }
  }

  /**
   * Use the [performAction] operation to execute the target of the action's business logic. This
   * will only run if the rules and validations are successful.
   */
  performAction() {
    this.loggingService.log(this.actionName, Severity.Information, `Preparing to .`);
    if (this.currentEvent && this.currentEvent.target && this.nextState) {
      this.response = this.businessProvider.createSuccessResponse(this.nextState, 'Successfully selected target event with state.');
    }
  }
}
