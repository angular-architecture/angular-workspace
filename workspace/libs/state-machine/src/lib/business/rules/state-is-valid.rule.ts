﻿import { CompositeRule, GuidIsValid, IsNotNullOrUndefined, IsTrue, RuleResult, StringIsNotNullEmptyRange } from '@valencia/rules-engine';

import { State } from '../../state';

/**
 * Use this rule to validate a string target. A valid string is not null or undefined; and it
 * is within the specified minimum and maximum length.
 */
export class StateIsValidRule<T> extends CompositeRule {
  /**
   * Use to provide the target [Primitive] to evaluate for the specified rule.
   */
  target: State<T>;

  /**
   * The constructor for the [StringIsNotNullEmptyRangeRule].
   * @param name The name of the rule.
   * @param message The message to display when the rule is violated.
   * @param target The target that the rule(s) will be evaluated against.
   * @param minLength The minimum allowed length of the target value.
   * @param maxLength The maximum allowed length of the target value.
   */
  constructor(name: string, message: string, target: State<T>, isDisplayable: boolean = false) {
    super(name, message, isDisplayable);
    this.target = target;
    this.configureRules();
  }

  /**
   * A helper method to configure/add rules to the validation context.
   */

  configureRules() {
    this.rules.push(new IsNotNullOrUndefined('StateIsNotNull', 'The State context is null or undefined.', this.target));

    if (this.target != null) {
      this.rules.push(new GuidIsValid('StateIdIsValid', 'The id value is not valid. Must be GUID.', this.target.id, true));
      this.rules.push(
        new StringIsNotNullEmptyRange(
          'StateNameIsValid',
          'The State context must have a valid name - between 1 and 100 characters.',
          this.target.name,
          1,
          100,
          true
        )
      );
      this.rules.push(new GuidIsValid('StateInitialSateIsValid', 'The State identifier must be a valid GUID.', this.target.id, true));
    }
  }
}
