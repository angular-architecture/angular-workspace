# AWS Cognito API Requests

## Security for Angular

Create a *Security* library project for the Angular workspace. This will be a feature library that contains components for security concerns. 

1. create account
2. verify account with code
3. login with credentials
4. forgot password
5. change password

Create the new library with the CLI:

```ts
ng generate @nrwl/angular:library --name=security --style=scss --buildable --importPath=@valencia/security --lazy --linter=eslint --publishable --routing --simpleModuleName
```

The CLI command will create and update the following files in the Angular workspace.

```ts
CREATE libs/security/.eslintrc.json (80 bytes)
CREATE libs/security/ng-package.json (158 bytes)
CREATE libs/security/package.json (198 bytes)
CREATE libs/security/README.md (140 bytes)
CREATE libs/security/tsconfig.lib.json (462 bytes)
CREATE libs/security/tsconfig.lib.prod.json (230 bytes)
CREATE libs/security/src/index.ts (39 bytes)
CREATE libs/security/src/lib/security.module.ts (336 bytes)
CREATE libs/security/tsconfig.json (517 bytes)
CREATE libs/security/jest.config.js (714 bytes)
CREATE libs/security/tsconfig.spec.json (233 bytes)
CREATE libs/security/src/test-setup.ts (30 bytes)
UPDATE angular.json (15321 bytes)
UPDATE nx.json (1126 bytes)
UPDATE tsconfig.base.json (1219 bytes)
UPDATE jest.config.js (434 bytes)
√ Packages installed successfully.
```

### Create Lazy-Loaded Components

```ts
ng generate @schematics/angular:module --name=login --project=security --module=security.module --route=login --routing
ng generate @schematics/angular:module --name=logout --project=security --module=security.module --route=logout --routing
ng generate @schematics/angular:module --name=create-account --project=security --module=security.module --route=create-account --routing
ng generate @schematics/angular:module --name=verify-account --project=security --module=security.module --route=verify-account --routing
ng generate @schematics/angular:module --name=change-password --project=security --module=security.module --route=change-password --routing
ng generate @schematics/angular:module --name=forgot-password --project=security --module=security.module --route=forgot-password --routing
```

Here is the output of the CLI for the `login` component module (SCAM).

```ts
ng generate @schematics/angular:module --name=login --project=security --module=security.module --route=login --routing

CREATE libs/security/src/lib/login/login-routing.module.ts (340 bytes)
CREATE libs/security/src/lib/login/login.module.ts (503 bytes)
CREATE libs/security/src/lib/login/login.component.html (20 bytes)
CREATE libs/security/src/lib/login/login.component.spec.ts (619 bytes)
CREATE libs/security/src/lib/login/login.component.ts (280 bytes)
CREATE libs/security/src/lib/login/login.component.scss (0 bytes)
UPDATE libs/security/src/lib/security.module.ts (450 bytes)
```

## Create Account

> SignUp: https://docs.aws.amazon.com/cognito-user-identity-pools/latest/APIReference/API_SignUp.html

Request URL: https://cognito-idp.us-west-2.amazonaws.com/
Request Method: POST
Status Code: 200 
Remote Address: 44.236.105.147:443
Referrer Policy: strict-origin-when-cross-origin

```json
{
    "ClientId": "6ngbf2etd3365oa1bm9amcln2k",
    "Username": "elena@buildmotion.com",
    "Password": "..Ilovetacos2020",
    "UserAttributes": []
}
``` 

Response:

```json
{
    "CodeDeliveryDetails": {
        "AttributeName": "email",
        "DeliveryMedium": "EMAIL",
        "Destination": "e***@b***.com"
    },
    "UserConfirmed": false,
    "UserSub": "777569b8-14d6-4b73-9105-3b12260584a7"
}
```

### Update Template With Container/Template

asdf

```html
<section id="security--create-account">
  <ng-container *ngIf="isAccountCreated$ | async; else createAccountFormTemplate">
    <h1>Account Created</h1>
    <p>Please check your email to verify your account.</p>
  </ng-container>
  <ng-template #createAccountFormTemplate>
    <!-- SHOW CREATE ACCOUNT FORM -->
    <h1>Create Account</h1>
  </ng-template>
</section>
```

### Create UI Service

Create a UI service to manage the state, data operations and UI/UX display logic. 

> Note: This keeps the component light-weight; focus on capture and display of
> information only.

```ts
ng g s createAccountUI --skip-tests
CREATE libs/security/src/lib/create-account/create-account-ui.service.ts (144 bytes)
```

Update the service with the *Observable/Subject*.

```ts
import { BehaviorSubject, Observable } from 'rxjs';

import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class CreateAccountUIService {
  private isAccountCreatedSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  public readonly isAccountCreated$: Observable<boolean> = this.isAccountCreatedSubject.asObservable();

  constructor() {}
}
```

### Initialize the FormGroup

```ts
// libs\security\src\lib\create-account\create-account.component.ts

  private passwordValidators = [Validators.required, Validators.minLength(8), Validators.maxLength(128)];

  initializeForm() {
    this.loggingService.log(this.componentName, Severity.Information, `Preparing to initialize the create account form.`);
    this.createAccountForm = this.formBuilder.group({
      emailAddress: new FormControl(undefined, {
        validators: [Validators.required, Validators.maxLength(100)],
        asyncValidators: [
          // ADD ASYNC EMAIL VALIDATION HERE;
        ],
        updateOn: 'blur',
      }),
      password: new FormControl(undefined, {
        validators: [...this.passwordValidators],
        asyncValidators: [
          // ADD PasswordStrength VALIDATION VIA SERVICE HERE
        ],
      }),
      passwordConfirm: new FormControl(undefined, {
        // validators: [...this.passwordValidators],
        updateOn: 'change',
      }),
    });
  }
```

## Verify Account with Code

Request URL: https://cognito-idp.us-west-2.amazonaws.com/
Request Method: POST
Status Code: 200 
Remote Address: 52.32.132.112:443
Referrer Policy: strict-origin-when-cross-origin

```json
{
    "ClientId": "6ngbf2etd3365oa1bm9amcln2k",
    "ConfirmationCode": "990765",
    "Username": "elena@buildmotion.com",
    "ForceAliasCreation": true
}
```

Response:

```json
{}
```

## Login with Credentials (User Name)

> Note that the password is not part of the request during the login workflow.

Request URL: https://cognito-idp.us-west-2.amazonaws.com/
Request Method: POST
Status Code: 200 
Remote Address: 35.164.22.230:443
Referrer Policy: strict-origin-when-cross-origin

```json
{
    "AuthFlow": "USER_SRP_AUTH",
    "ClientId": "6ngbf2etd3365oa1bm9amcln2k",
    "AuthParameters": {
        "USERNAME": "elena@buildmotion.com",
        "SRP_A": "4ad09f4f27d3fba89aefb690cba86e9ead34faaa26a544fb7981373b8e1199fe903aa033155859807dfc42840bf4e056e40158dbf9efc95e6091d59166aa5058e7ecc23257565655fc4bff414634da37eb2b85a7fe17153386e1197eb56ac4096898561b44fd0adb795b7dadca4b1ea7706bc2be89f1734e5a2a00e4919623346afa4bd1d54e9194770ab19e64a9210d8bf01151b7387a7a5aa9f161acdb57898e0240d617ad780d26b525a44f2adcb607e5834ffc33c6246495297c4e47ed4d4810142deb61b67606ef843942f3e758d141c728012ba07562a69bbf9e31c4bb52a6fcca5d0d952b27ccac24b01a84f736dce9b0a06c8a153e7072a76a41d52849227d52147fa96f1e6e2907eca47e600400c6e198c01b54522c8a89cfdc698d0b86be466fbf713c6ac761fa6136595ab5d4acf746fa0aa97f36617fbd22e717dfe339ad360b188b3142decb0f74cc897cea9d6cb53cb95e8d37d3118dcab0089d2da7b6aa23fa62ea7d37593a8c1a4432c2c15a2b8b9ca0445d06b8b702a340"
    },
    "ClientMetadata": {}
}
```

Response:

```json
{
    "ChallengeName": "PASSWORD_VERIFIER",
    "ChallengeParameters": {
        "SALT": "789dbf558b9b6f2b7724d939afa086e7",
        "SECRET_BLOCK": "T/XiuOfaGDdBuy48H8CIkOvs7w84SK5GSyot+grD3NpZ8F0/rUTKFDccaX7mc5Muzw/UufX1jr7DPfwDfJA7Bq97kuURU/PN1x1tBnu4gtKcL9x31F2+5gtMm5EBCPnndSqZtPG1NVtaUX89N1v3gKuEnWNRJglcBvSGsAgx5rHQHXgqhlQks46jo8pjiwmgaJRyhQ0VJ9JCQB+9dgZPKyKMwOWfcT8PI1FOwg/I65pMHliBLU5weKNbdcv9WPWGbR4milpGwqf+5QSr+bX2Sga/YperFf5A0a+B2uwQ8xT4QMjsxyqu1Abn0CL1INVwI3el/ft8Ix0jScwqfQ8+uUlJ6Ff7Nn0f0Zxg26IYyGQif9uSUipfrHxEMWFNMUuV9KQ+gApIb2n4Y3G9cvq6wJxEivkiujTpaJQ2HW5NjklwrO/mLiaG4AAPbx/LO12ixyEWJ5PPm80lSmiuDvGNZT22pFM/xVElR+PodUktPm2fRg00gci73KETPjZCefl0qzVC/3NtXBh89FKQxbAwu7aRXwZTgz0imNkpWiqbuts/higOlR1RmBkxJ8j9HbYidlqcuqpvhEgSrktp1/cT4v6gkCiX3E/7UC0sfRlJOcSpdi2iBaOhKH6L7qU/NtdkzHK3wvp0IWTLnYRfD+jvjTpGbRuYz1SZsO1UR350oSC80pZZK6nxj3s8AZVRBdHEoDhwDLuSBtc5N+BYOARyRp6Lck1yofLqt0JkEBXI5PgwSQ7ax75H/1pUnpE+xh3Q8VKgCD29iCHuleaK9o+CAOwuPRXGgE5B2/tuzuBl4I/tlT6fP8JW+/ipBjyRmX1xwqx8TEAp3qEfiv5vMVbocuLlLkBGFqwkplYims8am5k41AomTl1FSD3e08loG1Al6HjDWx32zD3jgGD1++ggPdsGiWAGuv6+qmwwd6hCC/3oxqYGFfYuziG/CpmEs26oNBpOg6QPILPp/9DNkWbnf4P4SyPvwtGntt/NCX0l8wyXQl6yx/OawOSp+b8K5xqnz9Yb8+Lftz0WxXQ5dN6oeljvR7OXQhRaCRPNuIjvimzWC4I8fnFN79PVv25GXcalkdH3ey7GyB85owACfL2jDjdx1vI6XkN/+AYSjCCnpNcpg4H7QQKsoWXhBuxtPFmChfeVcQqfx80aUtYnWGhnpnsEJGZ0LNXg/RfDG0MP5lUKlHgxw/eZO6uq3MzrCcdcrIbN5cEYER7xL9bunz9cakUP80NKH5Xfl7LQ2XT2JH6gQVUpp9rY6Ayrbd6kHsZ/65fATdLXA1SKAFt/Bacd297O5G+77fVk/NI8qZCy4luwzkau4X1Sinu/B7po+ubWJP0UmjtL6CS1kYAvgeqEues94uNjTQbV6DmDi7fWMd/Ihex8Fv0liZ+IrC99JZ7Z2bXF+J85G1GyA1DHzK8d9lxq94n+S0Fh7m696MinJTql3tiMPh1UGiRkpFnaVaLTnTPVcUP7UDjoTc5QR+P3cQJIAqGbgHqKXKRPGYam+j82XnlOjyZvMIBKfFiZufnVUdOXkdTbOs5hViSUMch7OcNIGxnOi67RH5fSGUFWjSXWdBYr1Mhh+t4J/pNwHtWp8zQMxBvxb4pVZEmhXSJ7RUPxx3k+PwwwhLRZrtBSI9An4XZ8WeeTVsJEQ0q/P7vdUU+SWXvx1TeHMkLLTkjCjXAIlOHEL8o7eU8whWxBPmhvU60Q4nAG8BRSHuo=",
        "SRP_B": "204ab8143d0ade34a98f583f1ee0a915d635ff6fd2b81c9d3d50a70aee7af2acdab216af65b89d6fae14a8ae283d93d2e5c16717a70133f9bc55b56ed9c5bf8a395884785a627c818e411c9bfc04c8a61aa0945d56978d04e5d9a813c3575b7a3bc3f4ab8b80d38f6bd41a7902887fbc3800de886c546ee6ad142cec3a05971171c56037654466c1e5ec952631543ba0ed0a33da8cdc8764d9ce38232c522c24ea966be70932e8548a18e3a8ae6dccad07fc6dac1e0dac736e30b8d83c51a8026ce120228893b740ae1ed641afababf68e54f863098c8e4fedf12b6c7266f58b01182517d930667ae707463d3a09f97ffb2158301720bd2e145cf4c35cddfb4611a1cefac68377d1ddfdccf5db5b1548fe76dc2b7ba5c9614054cc1b5292035bde1e579f247f4363dcdb4bdbc39ba8d6cb6b093d1ec750392ce78e5c19a3978a7bf3d0bdddbf87ef91fbef6835ef4eb9feaa81ddcbf55ef18d8813ba78d0a7b30ae0fa92bf1ea06851074a764fb684ed3ad00f7305970f9cba1fb6336b007b57",
        "USERNAME": "777569b8-14d6-4b73-9105-3b12260584a7",
        "USER_ID_FOR_SRP": "777569b8-14d6-4b73-9105-3b12260584a7"
    }
}
```

## Login :: Challenge

Request URL: https://cognito-idp.us-west-2.amazonaws.com/
Request Method: POST
Status Code: 200 
Remote Address: 35.164.22.230:443
Referrer Policy: strict-origin-when-cross-origin

```json
{
    "ChallengeName": "PASSWORD_VERIFIER",
    "ClientId": "6ngbf2etd3365oa1bm9amcln2k",
    "ChallengeResponses": {
        "USERNAME": "777569b8-14d6-4b73-9105-3b12260584a7",
        "PASSWORD_CLAIM_SECRET_BLOCK": "T/XiuOfaGDdBuy48H8CIkOvs7w84SK5GSyot+grD3NpZ8F0/rUTKFDccaX7mc5Muzw/UufX1jr7DPfwDfJA7Bq97kuURU/PN1x1tBnu4gtKcL9x31F2+5gtMm5EBCPnndSqZtPG1NVtaUX89N1v3gKuEnWNRJglcBvSGsAgx5rHQHXgqhlQks46jo8pjiwmgaJRyhQ0VJ9JCQB+9dgZPKyKMwOWfcT8PI1FOwg/I65pMHliBLU5weKNbdcv9WPWGbR4milpGwqf+5QSr+bX2Sga/YperFf5A0a+B2uwQ8xT4QMjsxyqu1Abn0CL1INVwI3el/ft8Ix0jScwqfQ8+uUlJ6Ff7Nn0f0Zxg26IYyGQif9uSUipfrHxEMWFNMUuV9KQ+gApIb2n4Y3G9cvq6wJxEivkiujTpaJQ2HW5NjklwrO/mLiaG4AAPbx/LO12ixyEWJ5PPm80lSmiuDvGNZT22pFM/xVElR+PodUktPm2fRg00gci73KETPjZCefl0qzVC/3NtXBh89FKQxbAwu7aRXwZTgz0imNkpWiqbuts/higOlR1RmBkxJ8j9HbYidlqcuqpvhEgSrktp1/cT4v6gkCiX3E/7UC0sfRlJOcSpdi2iBaOhKH6L7qU/NtdkzHK3wvp0IWTLnYRfD+jvjTpGbRuYz1SZsO1UR350oSC80pZZK6nxj3s8AZVRBdHEoDhwDLuSBtc5N+BYOARyRp6Lck1yofLqt0JkEBXI5PgwSQ7ax75H/1pUnpE+xh3Q8VKgCD29iCHuleaK9o+CAOwuPRXGgE5B2/tuzuBl4I/tlT6fP8JW+/ipBjyRmX1xwqx8TEAp3qEfiv5vMVbocuLlLkBGFqwkplYims8am5k41AomTl1FSD3e08loG1Al6HjDWx32zD3jgGD1++ggPdsGiWAGuv6+qmwwd6hCC/3oxqYGFfYuziG/CpmEs26oNBpOg6QPILPp/9DNkWbnf4P4SyPvwtGntt/NCX0l8wyXQl6yx/OawOSp+b8K5xqnz9Yb8+Lftz0WxXQ5dN6oeljvR7OXQhRaCRPNuIjvimzWC4I8fnFN79PVv25GXcalkdH3ey7GyB85owACfL2jDjdx1vI6XkN/+AYSjCCnpNcpg4H7QQKsoWXhBuxtPFmChfeVcQqfx80aUtYnWGhnpnsEJGZ0LNXg/RfDG0MP5lUKlHgxw/eZO6uq3MzrCcdcrIbN5cEYER7xL9bunz9cakUP80NKH5Xfl7LQ2XT2JH6gQVUpp9rY6Ayrbd6kHsZ/65fATdLXA1SKAFt/Bacd297O5G+77fVk/NI8qZCy4luwzkau4X1Sinu/B7po+ubWJP0UmjtL6CS1kYAvgeqEues94uNjTQbV6DmDi7fWMd/Ihex8Fv0liZ+IrC99JZ7Z2bXF+J85G1GyA1DHzK8d9lxq94n+S0Fh7m696MinJTql3tiMPh1UGiRkpFnaVaLTnTPVcUP7UDjoTc5QR+P3cQJIAqGbgHqKXKRPGYam+j82XnlOjyZvMIBKfFiZufnVUdOXkdTbOs5hViSUMch7OcNIGxnOi67RH5fSGUFWjSXWdBYr1Mhh+t4J/pNwHtWp8zQMxBvxb4pVZEmhXSJ7RUPxx3k+PwwwhLRZrtBSI9An4XZ8WeeTVsJEQ0q/P7vdUU+SWXvx1TeHMkLLTkjCjXAIlOHEL8o7eU8whWxBPmhvU60Q4nAG8BRSHuo=",
        "TIMESTAMP": "Wed Oct 28 21:29:39 UTC 2020",
        "PASSWORD_CLAIM_SIGNATURE": "9Dl2ZYZhRz9LXgujGSosqsvTUr2FXYxWGf+BoAfKKi0="
    }
}
```

Response:

```json 
{
    "AuthenticationResult": {
        "AccessToken": "eyJraWQiOiIrdlZRM0lKWDlUSlpHK1U5d09wdjRkbVhtTUVMdU1tUm9TNnlxOG1QXC9UUT0iLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiI3Nzc1NjliOC0xNGQ2LTRiNzMtOTEwNS0zYjEyMjYwNTg0YTciLCJldmVudF9pZCI6IjcwNzIzMzIwLWQzZDYtNDcxNy1hMjZjLWRkZjhhN2ZlODNmMiIsInRva2VuX3VzZSI6ImFjY2VzcyIsInNjb3BlIjoiYXdzLmNvZ25pdG8uc2lnbmluLnVzZXIuYWRtaW4iLCJhdXRoX3RpbWUiOjE2MDM5MjA1NzgsImlzcyI6Imh0dHBzOlwvXC9jb2duaXRvLWlkcC51cy13ZXN0LTIuYW1hem9uYXdzLmNvbVwvdXMtd2VzdC0yXzB3ZWJlQU5vdCIsImV4cCI6MTYwMzkyNDE3OCwiaWF0IjoxNjAzOTIwNTc4LCJqdGkiOiI3MTE2ZmYwMi1lZGZiLTQ1M2YtOWY3Yy0wNmIzMGM4MTVhNGMiLCJjbGllbnRfaWQiOiI2bmdiZjJldGQzMzY1b2ExYm05YW1jbG4yayIsInVzZXJuYW1lIjoiNzc3NTY5YjgtMTRkNi00YjczLTkxMDUtM2IxMjI2MDU4NGE3In0.LHgCnZXwSdgHTsNuvzSzrjpz1qyg09TdpiBXAlHX5HZGWEp_yxwa7fpRJrVtpGnKVFIp-jbSOtl-RYsNw19RWpoyvkxlwKAjFcV3lHmwtyF9j6qN7IY8vcV3eDG2oWdAK6mgPGKIiwvCcM0hMNbXerA8U2WwYy5pCMWgkXU6ylhfaKXN6r-BObicRT4RgFQou98_4JpG2ftMCzBoaeGRfYi1ZUV-Ak9bEgaD8BXHF8sgR6OPdySCR1FAD0O9Jypaq28l7DDwwAEJBfHlP8m_FodUNp9MMq6X7dkL35ziqlG5BqHXSNHQcZsCvGbwdopAwbI9rJiCc21PwkPrKcBWrA",
        "ExpiresIn": 3600,
        "IdToken": "eyJraWQiOiI1MTZxNzcyT3pDbnVVQ3BlUktnbHlRdEZoZTRJUFhtSVRxamtNbWRmN3lZPSIsImFsZyI6IlJTMjU2In0.eyJzdWIiOiI3Nzc1NjliOC0xNGQ2LTRiNzMtOTEwNS0zYjEyMjYwNTg0YTciLCJhdWQiOiI2bmdiZjJldGQzMzY1b2ExYm05YW1jbG4yayIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJldmVudF9pZCI6IjcwNzIzMzIwLWQzZDYtNDcxNy1hMjZjLWRkZjhhN2ZlODNmMiIsInRva2VuX3VzZSI6ImlkIiwiYXV0aF90aW1lIjoxNjAzOTIwNTc4LCJpc3MiOiJodHRwczpcL1wvY29nbml0by1pZHAudXMtd2VzdC0yLmFtYXpvbmF3cy5jb21cL3VzLXdlc3QtMl8wd2ViZUFOb3QiLCJjb2duaXRvOnVzZXJuYW1lIjoiNzc3NTY5YjgtMTRkNi00YjczLTkxMDUtM2IxMjI2MDU4NGE3IiwiZXhwIjoxNjAzOTI0MTc4LCJpYXQiOjE2MDM5MjA1NzgsImVtYWlsIjoiZWxlbmFAYnVpbGRtb3Rpb24uY29tIn0.Kbb_OSgzw2fEprVoYmUnI161542j6ioKehbSl-BUqLMZVooCOE4dp5LNDx41PuGeuL0SRWsvz38E07RHafISpO4hz8SLDllLkSGMBApdw_E6T4FKtL9cKjA89WVo8YPKuvT1xG6edrfCJwmVt-NWEpS06y6quGOh8qSxQHSGwD3i_879_v5kr1tPar6iwvJ_ndq8bUFWweoaR64ci7M8T66Cuh3yGzFS5CWWVeNl7EcCD9hOg7ANvaQ7kTdd-3ctvK037YmJLxxrEPMhiEYxn2fpdOx4iGpJTktnXLD0lD_XE5Pctko8bgfXzhxc6Ply6TFSQ7x_n0sU813KRVEWSw",
        "RefreshToken": "eyJjdHkiOiJKV1QiLCJlbmMiOiJBMjU2R0NNIiwiYWxnIjoiUlNBLU9BRVAifQ.tjQdzd4RJRE8e1OZ50rthmdD4WEAZdQPEZPNf0CmUnvSGgOcbdvnHAjbdnNYyAjHeBGcobya66CRc8rYbKnqIYSMk3zWcynXQco53f5u6f7tj99_iKuUGZ6wDn7S8Kcge0prMIEW9kkr32CFxns7ldUGZyvarSQ9cAgPXYhGRTKZi3PIVX9ow4uudTXNkVC0pS6ABNhQNO6LLackWk88_yN5-WldjwfLckWDMKVYsZHyRu4vBTI-Bsirn0qU4wn3wKve5Ibt8VZuW8steQ9-N1JpW58T25jQ1HtJ5Io2wuQiGIcgqa36x5P_OXV6HFPV8QTwSPA5YIBJObLPt4Kraw.3YIh-uIijJjtTJTJ.GFlPmMt5sXga74pL3XIzm3olytiiW-Mz5ragTBA4nQqqrx-gXynZ36THDg4hEv6VoqSgm5Q5DKh75adCb-F9llg3e4CC_S8ZsvRQFxb1Audl5gPFe7KjUv8cZknN1oViXjm3Ppk1l5xY8uoc11dczY18sV9pfT6WvYCFv1MF6iAFxCknMA9zg4NDiHuETvcQIVIT0cJ2R8V3tESg6Y13boEppW3GOSVUOx6Kgyf_tdZRyz2PKD6ELayyN7bx48xvVSDmrRCW164acMjVlvZXiVR1v48FUdQhktRKo7TlJew0RN36H1yceuz_enXsas7XGZzAL9TPKBvCde1b6wPm7w2_2Sj58NO4qlYaEmXVRQ8wL6xWQYAHwXwSE0ZO0XmGAN5IA9LK84gmNt0UNePhEYwYbFkyrzvDog6mHWrQ7kPtmv24bHdUQm1i7tgjl400FsUvO3FCWd3ofpxI8fnQ8-Rc3WKY1Fdjpoj-JqmFVhuEzBomArnu5chGNGI_ddznwdfpkpr-9S1ekA8M6WkUP0w23T4TUVSq4KBUGQOOrCn82duiqn61vIpUqGXe_dOu1EG3nCMrjcIK2UJmIcyrl4W3QBJKRnI5ktwAjhke_EStJu5wrjVsdmsqoQPJcZyD9pGGFAwtmsLkRF9yEm6p62mAalj0sGvO2jtjIjSStXNLFkr1wa58IsS3DrvbyDz05g1VCvFPteEdPxkADJEsxqdr2n3uSR-c2viN-axC-Zydf03w7x_ytfvBngKbBsk9EmVYN8AmCHFTgYhAZTuoTmjm_q-9sRfkFCu2CR62TfnM5Pl-90W4M1SMMffGuhFFRmgdQznllD_G5ki748mMQ73dK2M4O3UprVeGTNvH3QCoRpoxHeNKyhRhoA76_M7F6ZR_bZZLqlNXxPlNYEcA6XjDY9n7M-8VShOkR63cmt2Tn4GWfyFKsUYx99CIH2liFqT6sDsKuFX4cCEgAxqpbLw1_Uhqxz5O_4mE5LwA_USPiq19rK8DERR5Mhzl2LWjgL6tTeo05EcfEyxRMoZrpzIBGtf1QTz6t9dyyLZ60nSgL9hkRDquka9IvfX-uZ6Lr1X4fmwSfBSIhvVgCYZzKH9jR6TwYrdCKJxk06ov3Ac7RIlwxABSdYJ54OY0LGzDIPvrktbp75YwQOSxuPZ0v7e2p4Fv9AaB2enif6CN06-pLfQS-fhnVFZxNEpcH5X9G7bGMfhuBO1SVctObA1vFjLPBVgJYRs3IHIjDnJVNuQjMv3iYdnb02shj9aAb6rFybhrxZ7qTVxOY0EcVUcY-HRXkGLEi9__LwZZi7mFyZL-otUy2QIcNbie5w.6KXeXgOI3RuydJMWVCUdhw",
        "TokenType": "Bearer"
    },
    "ChallengeParameters": {}
}
```

## Resources

- [Configuring a User Pool App Client](https://docs.aws.amazon.com/cognito/latest/developerguide/cognito-user-pools-app-idp-settings.html)
- [Amplify Docs::Sign Up, Sign In & Sign Out](https://docs.amplify.aws/lib/auth/emailpassword/q/platform/js)
- [AWS Cognito with Angular YouTube](https://www.youtube.com/watch?v=ROwjNYlxMAs)
  - [Github.com repository](https://github.com/kousekt/angularcognitotest)
- [Using Cognito for users management in your Serverless application](https://medium.com/@Da_vidgf/using-cognito-for-users-management-in-your-serverless-application-1695fec9e225)
  - [Github.com: davidgf/serverless-cognito](https://github.com/davidgf/serverless-cognito/blob/master/serverless.yml#L19)