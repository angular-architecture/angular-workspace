# Creatr Assets

```ts
ng generate @nrwl/angular:library --name=assets --style=scss --directory=creatr --importPath=@valencia/creatr/assets --linter=eslint --publishable --simpleModuleName <

CREATE libs/creatr/assets/.eslintrc.json (83 bytes)
CREATE libs/creatr/assets/ng-package.json (169 bytes)
CREATE libs/creatr/assets/package.json (203 bytes)
CREATE libs/creatr/assets/README.md (150 bytes)
CREATE libs/creatr/assets/tsconfig.lib.json (465 bytes)
CREATE libs/creatr/assets/tsconfig.lib.prod.json (230 bytes)
CREATE libs/creatr/assets/src/index.ts (37 bytes)
CREATE libs/creatr/assets/src/lib/assets.module.ts (162 bytes)
CREATE libs/creatr/assets/tsconfig.json (254 bytes)
CREATE libs/creatr/assets/jest.config.js (730 bytes)
CREATE libs/creatr/assets/tsconfig.spec.json (236 bytes)
CREATE libs/creatr/assets/src/test-setup.ts (30 bytes)
UPDATE angular.json (17585 bytes)
UPDATE nx.json (1217 bytes)
UPDATE tsconfig.base.json (1353 bytes)
UPDATE jest.config.js (503 bytes)
√ Packages installed successfully.
```

The angular.json configuration is updated with the project information. Since the assets
are consumed by Angular applications as a resource - there is no requirement to build or
test the assets. Typically, the assets are files for a website:

- font
- stylesheet files (CSS, SCSS),
- images (.png, .jpg, .svg, )
- JavaScript
- icons
- favicon.ico
- JSON

We can safely  remove the *build*, *lint*, and *test* nodes of the `architect` configuration for the library
project

```json
    "creatr-assets": {
      "projectType": "library",
      "root": "libs/creatr/assets",
      "sourceRoot": "libs/creatr/assets/src",
      "prefix": "buildmotion",
      "architect": {
        // "build": {
        //   "builder": "@nrwl/angular:package",
        //   "options": {
        //     "tsConfig": "libs/creatr/assets/tsconfig.lib.json",
        //     "project": "libs/creatr/assets/ng-package.json"
        //   },
        //   "configurations": {
        //     "production": {
        //       "tsConfig": "libs/creatr/assets/tsconfig.lib.prod.json"
        //     }
        //   }
        // },
        // "lint": {
        //   "builder": "@nrwl/linter:eslint",
        //   "options": {
        //     "lintFilePatterns": ["libs/creatr/assets/src/**/*.ts"]
        //   }
        // },
        // "test": {
        //   "builder": "@nrwl/jest:jest",
        //   "options": {
        //     "jestConfig": "libs/creatr/assets/jest.config.js",
        //     "passWithNoTests": true
        //   }
        // }
      },
      "schematics": {
        "@schematics/angular:component": {
          "style": "scss"
        }
      }
    }
```